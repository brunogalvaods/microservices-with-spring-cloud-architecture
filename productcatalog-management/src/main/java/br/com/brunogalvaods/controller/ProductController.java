package br.com.brunogalvaods.controller;

import br.com.brunogalvaods.domain.Product;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
public class ProductController {

    @GetMapping(value = "/products", produces = "application/json")
    public ResponseEntity<List<Product>> getProducts() {
        try {
            List<Product> products = new ArrayList<>();
            products.add(Product.builder()
                    .id(UUID.randomUUID().toString())
                    .name("Laptop")
                    .description("product description")
                    .price(BigDecimal.valueOf(2000.00))
                    .build());
            return new ResponseEntity<>(products, HttpStatus.OK);
        } catch (final Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
